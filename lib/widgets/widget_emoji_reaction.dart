// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter_reaction_button/flutter_reaction_button.dart';
import 'package:hugolina/widgets/widget_input.dart';
import 'package:utils_libs/utils_libs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class WidgetEmojiReaction extends StatefulWidget {
  WidgetEmojiReaction({Key? key}) : super(key: key);

  @override
  _WidgetEmojiReactionState createState() => _WidgetEmojiReactionState();
}

class _WidgetEmojiReactionState extends State<WidgetEmojiReaction> {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: FlutterReactionButtonCheck(
          onReactionChanged: (reaction, index, isChecked) {
            print('reaction selected index: $index');
          },
          reactions: <Reaction>[
            Reaction(
              previewIcon: buildWidgetPreview(
                icon: ICONS.ICON_LIKE_GIF,
              ),
              icon: buildWidget(
                icon: ICONS.ICON_LIKE,
                text: Text(
                  MESSAGES.LIKE,
                  style: AppStyle.DEFAULT_SMALLs.copyWith(color: COLORS.PRIMARY_COLOR),
                ),
              ),
            ),
            Reaction(
              previewIcon: buildWidgetPreview(
                icon: ICONS.ICON_LOVE_GIF,
              ),
              icon: buildWidget(
                icon: ICONS.ICON_LOVE,
                text: Text(
                  MESSAGES.LOVE,
                  style: AppStyle.DEFAULT_SMALLs.copyWith(color: COLORS.RED),
                ),
              ),
            ),
            Reaction(
              previewIcon: buildWidgetPreview(
                icon: ICONS.ICON_WOW_GIF,
              ),
              icon: buildWidget(
                icon: ICONS.ICON_WOW,
                text: Text(
                  MESSAGES.WOW,
                  style: AppStyle.DEFAULT_SMALLs.copyWith(color: COLORS.REACTION),
                ),
              ),
            ),
            Reaction(
              previewIcon: buildWidgetPreview(
                icon: ICONS.ICON_HAHA_GIF,
              ),
              icon: buildWidget(
                icon: ICONS.ICON_HAHA,
                text: Text(
                  MESSAGES.HAHA,
                  style: AppStyle.DEFAULT_SMALLs.copyWith(color: COLORS.REACTION),
                ),
              ),
            ),
            Reaction(
              previewIcon: buildWidgetPreview(
                icon: ICONS.ICON_SAD_GIF,
              ),
              icon: buildWidget(
                icon: ICONS.ICON_SAD,
                text: Text(
                  MESSAGES.SAD,
                  style: AppStyle.DEFAULT_SMALLs.copyWith(color: COLORS.REACTION),
                ),
              ),
            ),
            Reaction(
              previewIcon: buildWidgetPreview(
                icon: ICONS.ICON_ANGRY_GIF,
              ),
              icon: buildWidget(
                icon: ICONS.ICON_ANGRY,
                text: Text(
                  MESSAGES.ANGRY,
                  style: AppStyle.DEFAULT_SMALLs.copyWith(color: COLORS.RED),
                ),
              ),
            ),
          ],
          initialReaction: Reaction(
            icon: buildWidget(
              icon: ICONS.ICON_UNLIKE,
              text: Text(
                  MESSAGES.LIKE,
                  style: AppStyle.DEFAULT_SMALLs.copyWith(color: COLORS.BLACK)
              ),
            ),
          ),
          selectedReaction: Reaction(
            icon: buildWidget(
              icon: ICONS.ICON_LIKE,
              text: Text(
                MESSAGES.LIKE,
                style: AppStyle.DEFAULT_SMALLs.copyWith(color: COLORS.PRIMARY_COLOR),
              ),
            ),
          ),
        )
    );
  }

  Widget buildWidgetPreview({required String icon}) => Padding(
    padding: const EdgeInsets.symmetric(horizontal: 3.5, vertical: 5),
    child: Image.asset(icon, height: 40),
  );

  Widget buildWidget({required String icon, required Text text}) => Container(
    color: Colors.transparent,
    child: Row(
      children: <Widget>[
        Image.asset(icon, height: 20),
        const SizedBox(width: 5),
        text,
      ],
    ),
  );
}
