// ignore: import_of_legacy_library_into_null_safe
import 'package:hugolina/widgets/widgets.dart';
import 'package:utils_libs/utils_libs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:formz/formz.dart';

class WidgetPostCommentItem extends StatefulWidget {
  @override
  _WidgetPostCommentItemState createState() => _WidgetPostCommentItemState();
}

class _WidgetPostCommentItemState extends State<WidgetPostCommentItem> {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: COLORS.WHITE,
      padding: EdgeInsets.only(top: 5, bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          WidgetImageCircle(
            backgroundColor: COLORS.RED,
            height: 35,
            width: 35,
          ),
          AppValue.hSpaceTiny,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              WidgetColumnTwoItem(
                padding: 5,
                boxDecoration: BoxDecoration(
                    color: COLORS.LIGHT_GREY,
                    borderRadius: BorderRadius.circular(6)
                ),
                textUp: 'Ann Ann',
                textDown: 'Lorem ipsum dolor sit amet',
              ),
              AppValue.vSpaceTiny,
              WidgetRowTwoItem(
                widgetLeft: InkWell(
                  child: Text(MESSAGES.LIKE, style: AppStyle.DEFAULT_SMALLs,),
                ),
                widgetRight: InkWell(
                  child: Text(MESSAGES.REPLY, style: AppStyle.DEFAULT_SMALLs,),
                ),
              ),
            ],
          ),


        ],
      ),
    );
  }
}
