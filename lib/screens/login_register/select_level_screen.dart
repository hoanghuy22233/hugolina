import 'package:flutter/cupertino.dart';
import 'package:flutter/painting.dart';
import 'package:hugolina/screens/screens.dart';
import 'package:hugolina/widgets/widgets.dart';
import 'package:utils_libs/utils_libs.dart'; // ignore: import_of_legacy_library_into_null_safe
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';

class SelectLevelScreen extends StatefulWidget {
  @override
  _SelectLevelScreenState createState() => _SelectLevelScreenState();
}

class _SelectLevelScreenState extends State<SelectLevelScreen> {

  late List<ButtonModel> level = [
    ButtonModel(
      title: MESSAGES.EASY_LEVEL,
      isCheckRender: false,
    ),
    ButtonModel(
      title: MESSAGES.MEDIUM_LEVEL,
      isCheckRender: false,
    ),
    ButtonModel(
      title: MESSAGES.HARD_LEVEL,
      isCheckRender: false,
    ),
  ];

  String levelUser = '';

  @override
  Widget build(BuildContext context) {
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: SafeArea(
          child: AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.all(0),
            content: Container(
              width: AppValue.widths,
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                      child: Column(
                        children: [
                          WidgetContainerCenter(
                            width: AppValue.widths,
                            height: 40,
                            boxDecoration: BoxDecoration(
                              color: COLORS.PRIMARY_COLOR,
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(6), topRight: Radius.circular(6))
                            ),
                            child: Center(child: Text(MESSAGES.SELECT_LEVEL_TITLE, style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: COLORS.WHITE))),
                          ),
                          AppValue.vSpaceSmall,
                          Expanded(
                            child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              itemCount: level.length,
                              itemBuilder: (context, index) {
                                List<ButtonModel> list = [];
                                return Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                                  child: WidgetButton(
                                      onTap: () {
                                        print(level[index].title);
                                        AppNavigator.navigateMain();
                                      },
                                      text: level[index].title,
                                      backgroundColor: COLORS.WHITE,
                                      isBorder: true,
                                      borderColor: COLORS.GREY,
                                      textColor: COLORS.BLACK,
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      )
                  ),
                ],
              ),
            ),
          )
        ),
      ),
    );
  }
}
