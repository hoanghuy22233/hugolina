// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter/scheduler.dart';
import 'package:flutter_reaction_button/flutter_reaction_button.dart';
import 'package:hugolina/widgets/widgets.dart';
import 'package:utils_libs/utils_libs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DiscoverItem extends StatefulWidget {
  final String title;

  const DiscoverItem({Key? key, required this.title}) : super(key: key);

  @override
  _DiscoverItemState createState() => _DiscoverItemState();
}

class _DiscoverItemState extends State<DiscoverItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            widget.title,
            style: AppStyle.DEFAULT_SMALLs,
          ),
        ),
      ),
    );
  }

}
