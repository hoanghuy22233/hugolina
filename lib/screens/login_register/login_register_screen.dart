import 'package:flutter/cupertino.dart';
import 'package:hugolina/screens/screens.dart';
import 'package:hugolina/widgets/widgets.dart';
import 'package:utils_libs/utils_libs.dart'; // ignore: import_of_legacy_library_into_null_safe
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';

class LoginAndRegisterScreen extends StatefulWidget {
  @override
  _LoginAndRegisterScreenState createState() => _LoginAndRegisterScreenState();
}

class _LoginAndRegisterScreenState extends State<LoginAndRegisterScreen>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 3);
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: SafeArea(
          child: AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.only(top: 10.0),
            content: Container(
              width: AppValue.widths,
              height: AppValue.heights*0.7,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildTabBarMenu(),
                  Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
                        child: TabBarView(
                          controller: _tabController,
                          children: [
                            LoginScreen(),
                            RegisterScreen(),
                            Container()
                          ],
                        ),
                      )
                  ),
                ],
              ),
            ),
          )
        ),
      ),
    );
  }

  Widget _buildTabBarMenu() {
    return Container(
      height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        tabs: [
          Tab(
            text: MESSAGES.LOGIN,
          ),
          Tab(
            text: MESSAGES.REGISTER,
          ),
          WidgetBackButton()
        ],
        labelStyle: AppStyle.DEFAULT_SMALL_BOLD,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: COLORS.WHITE,
        unselectedLabelColor: COLORS.BLACK,
        indicatorSize: TabBarIndicatorSize.tab,
        indicator: new BubbleTabIndicator(
          indicatorHeight: 35,
          indicatorColor: COLORS.PRIMARY_COLOR,
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
          indicatorRadius: 6,
        ),
      ),
    );
  }
}
