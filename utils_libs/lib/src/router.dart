// ignore: camel_case_types
class ROUTE_NAMES {
  static const String SPLASH = '/splash';
  static const String LOGIN = '/login';
  static const String MENU = 'menu';
  static const String MAIN = '/main';
  static const String HOME = '/home';
  static const String DISCOVER = '/DISCOVER';
  static const String COMMUNITY = '/COMMUNITY';
  static const String NOTIFICATION = '/NOTIFICATION';
  static const String USER = '/USER';
}