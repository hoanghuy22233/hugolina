import 'package:flutter/material.dart';
class COLORS {
  COLORS._();
  static const  BLACK = const Color(0xFF000000);
  static const  WHITE = const Color(0xffffffff);
  static const  GREY = const Color(0xFF808080);
  static const  PRIMARY_COLOR = const Color(0xFF1877F2);
  static const  RED = const Color(0xffff0000);
  static const  YELLOW = const Color(0xffd6ff00);
  static const  GREEN = const Color(0xff008000);
  static const  LIGHT_GREEN = const Color(0xff14cb3e);
  static const  TRUE_GREEN = const Color(0xff08a413);
  static const  ORANGE = Color(0xffffa500);
  static const  NAV_ITEM_COLOR = Color(0xff515151);
  static const  LOGIN_GG = Color(0xffDF4B38);
  static const  LOGIN_FB = Color(0xff3E5C9A);
  static const  CHECK_LEVEL = Color(0xff80C3FF);
  static const  LIGHT_GREY = Color(0xffE8E8E8);
  static const  TRANSLATE = Color(0xffA9DAFC);
  static const  REACTION = Color(0xffffda6b);
}
