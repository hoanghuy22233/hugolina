// ignore: import_of_legacy_library_into_null_safe
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:hugolina/screens/screens.dart';
import 'package:utils_libs/utils_libs.dart'; // ignore: import_of_legacy_library_into_null_safe
import 'package:flutter/material.dart';
import 'package:hugolina/widgets/widgets.dart';
class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0.0,
        automaticallyImplyLeading: false,
        backgroundColor: COLORS.PRIMARY_COLOR,
        elevation: 0.0,
      ),
      body: Scaffold(
        body: DoubleBackToCloseApp(
          child: NavigationButton(
            selectedIndex: _selectedIndex,
            onSelectIndex: () => setState(() => _selectedIndex = 2 ),
            onTabSelected: (index) => setState(() => _selectedIndex = index),
            children: [
              HomeScreen(),
              DiscoverScreen(),
              CommunityScreen(),
              NotificationScreen(),
              UserScreen(),
            ],
          ),
          snackBar: SnackBar(
            content: Text(MESSAGES.BACK_TO_EXIT, style: AppStyle.DEFAULT_MEDIUM.copyWith(color: COLORS.WHITE),),
          ),
        ),
      ),
    );
  }

}