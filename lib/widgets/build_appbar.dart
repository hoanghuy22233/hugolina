// ignore: import_of_legacy_library_into_null_safe
import 'package:hugolina/widgets/widgets.dart';
import 'package:utils_libs/utils_libs.dart' show COLORS;
import 'package:flutter/material.dart';

class WidgetHeaderBar extends StatelessWidget {
  final String title;
  final String? icon;
  final List<Widget>? right;
  final GlobalKey<ScaffoldState>? drawerKey;

  WidgetHeaderBar({required this.title, this.icon, this.right, this.drawerKey});

  @override
  Widget build(BuildContext context) {
    return WidgetAppbar(
      textColor: COLORS.WHITE,
      left: [
        WidgetBackButton(
          icon: icon,
          onTap: drawerKey != null
              ? () => drawerKey!.currentState!.openDrawer()
              : null,
        ),
      ],
      right: right,
      title: title,
    );
  }
}
