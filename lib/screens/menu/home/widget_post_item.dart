// ignore: import_of_legacy_library_into_null_safe
import 'package:hugolina/screens/screens.dart';
import 'package:hugolina/widgets/widgets.dart';
import 'package:utils_libs/utils_libs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class WidgetPostItem extends StatefulWidget {
  @override
  _WidgetPostItemState createState() => _WidgetPostItemState();
}

class _WidgetPostItemState extends State<WidgetPostItem> {

  final _commentFocusNode = FocusNode();
  bool like = false, useful = false;



  @override
  Widget build(BuildContext context) {
    return Container(
      color: COLORS.WHITE,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: WidgetImageCircle(
                  height: 35,
                  width: 35,
                ),
              ),
              AppValue.hSpaceTiny,
              Expanded(
                flex: 6,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(MESSAGES.READ_ENGLISH, style: AppStyle.DEFAULT_MEDIUM_BOLD,),
                    Text(MESSAGES.DATE_CREATE, style: AppStyle.DEFAULT_SMALLs,),
                  ],
                )
              ),
              AppValue.hSpaceTiny,
              Expanded(
                flex: 2,
                child: WidgetButton(
                  onTap: (){},
                  backgroundColor: COLORS.LIGHT_GREY,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.add, color: COLORS.PRIMARY_COLOR, size: 15,),
                      Text(MESSAGES.FOLLOW, style: AppStyle.DEFAULT_SMALL.copyWith(color: COLORS.PRIMARY_COLOR),),
                    ],
                  ),
                ),
              )
            ],
          ),
          AppValue.vSpaceSmall,
          Text(MESSAGES.POST_CONTENT_TEXT, style: AppStyle.DEFAULT_SMALLs),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              WidgetContainerImage(
                width: AppValue.widths*0.6,
                image: ICONS.PLAY_AUDIO,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  WidgetContainerImage(height: 25, width: 25, image: ICONS.ICON_VOCABULARY,),
                  AppValue.hSpaceTiny,
                  WidgetContainerImage(height: 25, width: 25, image: ICONS.ICON_QUESTION,),
                ],
              ),
            ],
          ),
          WidgetContainerImage(
            height: AppValue.widths-40,
            width: AppValue.widths,
            image: IMAGES.POST_IMAGE,
          ),
          Container(
            decoration: BoxDecoration(
              color: COLORS.TRANSLATE
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(MESSAGES.TRANSLATE, style: AppStyle.DEFAULT_SMALLs_BOLD,),
                Text(MESSAGES.POST_CONTENT_TEXT, style: AppStyle.DEFAULT_SMALLs),
              ],
            ),
          ),
          Divider(
            thickness: 1,
          ),
          Container(
            height: 20,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 80,
                  child: WidgetEmojiReaction()
                ),
                WidgetRowTwoItem(
                  widgetLeft: WidgetContainerImage(height: 15, width: 15, image: ICONS.ICON_COMMENT,),
                  textRight: MESSAGES.COMMENT,
                  width: 80,
                ),
                InkWell(
                  onTap: (){
                    setState(() {
                      useful = !useful;
                    });
                  },
                  child: WidgetRowTwoItem(
                    widgetLeft: WidgetContainerImage(height: 15, width: 15, image: useful ? ICONS.ICON_USEFUL : ICONS.ICON_UN_USEFUL,),
                    textRight: MESSAGES.USEFUL,
                    width: 80,
                  ),
                ),
                WidgetRowTwoItem(
                  widgetLeft: WidgetContainerImage(height: 15, width: 15, image: ICONS.ICON_SHARE,),
                  textRight: MESSAGES.SHARE,
                  width: 80,
                ),
              ],
            ),
          ),
          Divider(
            thickness: 1,
          ),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: 2,
            itemBuilder: (context, index) {
              return WidgetPostCommentItem();
            },
          ),
          Row(
            children: [
              Expanded(
                child: WidgetImageCircle(
                  height: 35,
                  width: 35,
                ),
              ),
              AppValue.hSpaceTiny,
              Expanded(
                flex: 9,
                child: WidgetInput(
                  hint: MESSAGES.WRITE_COMMENT,
                  onChanged: (value) => null,
                  inputType: TextInputType.text,
                  focusNode: _commentFocusNode,
                ),
              ),
            ],
          ),
          Divider(
            thickness: 5,
          ),
          AppValue.vSpaceSmall
        ],
      ),
    );
  }
}
