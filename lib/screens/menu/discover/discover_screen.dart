// ignore: import_of_legacy_library_into_null_safe
import 'package:hugolina/screens/screens.dart';
import 'package:hugolina/widgets/widgets.dart';
import 'package:utils_libs/utils_libs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DiscoverScreen extends StatefulWidget {

  DiscoverScreen({Key? key}) : super(key: key);

  @override
  _DiscoverScreenState createState() => _DiscoverScreenState();
}

class _DiscoverScreenState extends State<DiscoverScreen> {

  List<String> items = [
    'Funny',
    'Fun fact',
    'Ý nghĩa cuộc sống',
    'Did you know',
    'Joker',
    'Funny Story',
    'Travel',
    'Funny conversation',
    'Động lực',
    'Famous Peson',
    'Movie',
    'Quote',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
            color: COLORS.WHITE,
            padding: EdgeInsets.all(10),
            child: GridView.builder(
              itemCount: items.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 4.0,
                mainAxisSpacing: 4.0,
                childAspectRatio: 3
              ),
              itemBuilder: (BuildContext context, int index) {
                return DiscoverItem(title: items[index],);
              },
            ),
          )
      ),
    );
  }

}
