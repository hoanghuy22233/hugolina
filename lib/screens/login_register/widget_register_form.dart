// ignore: import_of_legacy_library_into_null_safe
import 'package:hugolina/screens/screens.dart';
import 'package:hugolina/widgets/widgets.dart';
import 'package:utils_libs/utils_libs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:formz/formz.dart';

class WidgetRegisterForm extends StatefulWidget {
  @override
  _WidgetRegisterFormState createState() => _WidgetRegisterFormState();
}

class _WidgetRegisterFormState extends State<WidgetRegisterForm> {
  final _fullnameFocusNode = FocusNode();
  final _emailFocusNode = FocusNode();
  final _passwordFocusNode = FocusNode();
  final _rePasswordFocusNode = FocusNode();
  bool obscurePassword = true;
  bool obscureRePassword = true;

  @override
  void initState() {
    super.initState();
    _fullnameFocusNode.addListener(() {
      if (!_fullnameFocusNode.hasFocus) {
        FocusScope.of(context).requestFocus(_emailFocusNode);
      }
    });
    _emailFocusNode.addListener(() {
      if (!_emailFocusNode.hasFocus) {
        FocusScope.of(context).requestFocus(_passwordFocusNode);
      }
    });
    _passwordFocusNode.addListener(() {
      if (!_passwordFocusNode.hasFocus) {
        FocusScope.of(context).requestFocus(_rePasswordFocusNode);
      }
    });
    _rePasswordFocusNode.addListener(() {
      if (!_rePasswordFocusNode.hasFocus) {
      }
    });
  }

  @override
  void dispose() {
    _emailFocusNode.dispose();
    _fullnameFocusNode.dispose();
    _passwordFocusNode.dispose();
    _rePasswordFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: COLORS.WHITE,
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Form(
        child: SingleChildScrollView(
          child: Column(
            children: [
              _buildTextFieldFullname(),
              AppValue.vSpaceTiny,
              _buildTextFieldEmail(),
              AppValue.vSpaceTiny,
              _buildTextFieldPassword(),
              AppValue.vSpaceTiny,
              _buildTextFieldRePassword(),
              AppValue.vSpaceSmall,
              _buildButtonRegister(),
              AppValue.vSpaceTiny,
              Text(
                MESSAGES.OR,
                style: AppStyle.DEFAULT_SMALL,
              ),
              AppValue.vSpaceTiny,
              _buildButtonRegisterGoogle(),
              AppValue.vSpaceTiny,
              _buildButtonRegisterFacebook()
            ],
          ),
        ),
      ),
    );
  }

  _buildButtonRegister() {
    return WidgetButton(
      onTap: () => showDialog(
        context: context,
        builder: (BuildContext context) {
          return SelectLevelScreen();
        },
      ),
      enable: true,
      backgroundColor: COLORS.PRIMARY_COLOR,
      text: MESSAGES.REGISTER,
    );
  }

  _buildButtonRegisterGoogle() {
    return WidgetButton(
      onTap: () => null,
      enable: true,
      backgroundColor: COLORS.LOGIN_GG,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          WidgetContainerImage(
            image: ICONS.ICON_GG,
          ),
          Text(MESSAGES.LOGIN_WITH_GG, style: AppStyle.DEFAULT_MEDIUM.copyWith(color: COLORS.WHITE))
        ],
      ),
    );
  }

  _buildButtonRegisterFacebook() {
    return WidgetButton(
      onTap: () => null,
      enable: true,
      backgroundColor: COLORS.LOGIN_FB,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          WidgetContainerImage(
            image: ICONS.ICON_FB,
          ),
          Text(MESSAGES.LOGIN_WITH_FB, style: AppStyle.DEFAULT_MEDIUM.copyWith(color: COLORS.WHITE))
        ],
      ),
    );
  }

  _buildTextFieldFullname() {
    return WidgetInput(
      onChanged: (value) => null,
      inputType: TextInputType.text,
      focusNode: _fullnameFocusNode,
      hint: MESSAGES.FULLNAME,
    );
  }

  _buildTextFieldEmail() {
    return WidgetInput(
      onChanged: (value) => null,
      inputType: TextInputType.emailAddress,
      focusNode: _emailFocusNode,
      hint: MESSAGES.EMAIL_OR_PHONE,
    );
  }

  _buildTextFieldPassword() {
    return WidgetInput(
      onChanged: (value) => null,
      hint: MESSAGES.PASSWORD,
      obscureText: obscurePassword,
      focusNode: _passwordFocusNode,
      endIcon: InkWell(
        onTap: () => setState(() => obscurePassword = !obscurePassword),
        child: Icon(
          obscurePassword
              ? CommunityMaterialIcons.eye_outline
              : CommunityMaterialIcons.eye_off_outline,
          color: COLORS.GREY,
          size: 20,
        ),
      ),
    );
  }

  _buildTextFieldRePassword() {
    return WidgetInput(
      onChanged: (value) => null,
      hint: MESSAGES.PASSWORD,
      obscureText: obscureRePassword,
      focusNode: _rePasswordFocusNode,
      endIcon: InkWell(
        onTap: () => setState(() => obscureRePassword = !obscureRePassword),
        child: Icon(
          obscureRePassword
              ? CommunityMaterialIcons.eye_outline
              : CommunityMaterialIcons.eye_off_outline,
          color: COLORS.GREY,
          size: 20,
        ),
      ),
    );
  }
}
