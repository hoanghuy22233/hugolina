import 'package:equatable/equatable.dart';
import 'package:utils_libs/utils_libs.dart';

class ButtonModel {
  final String title;
  final bool isCheckRender;
  ButtonModel({this.title, this.isCheckRender});
}