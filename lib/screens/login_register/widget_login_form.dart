// ignore: import_of_legacy_library_into_null_safe
import 'package:hugolina/widgets/widgets.dart';
import 'package:utils_libs/utils_libs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:formz/formz.dart';

class WidgetLoginForm extends StatefulWidget {
  @override
  _WidgetLoginFormState createState() => _WidgetLoginFormState();
}

class _WidgetLoginFormState extends State<WidgetLoginForm> {
  final _emailFocusNode = FocusNode();
  final _passwordFocusNode = FocusNode();
  bool obscurePassword = true;

  @override
  void initState() {
    super.initState();
    _emailFocusNode.addListener(() {
      if (!_emailFocusNode.hasFocus) {
        FocusScope.of(context).requestFocus(_passwordFocusNode);
      }
    });
    _passwordFocusNode.addListener(() {
      if (!_passwordFocusNode.hasFocus) {
      }
    });
  }

  @override
  void dispose() {
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: COLORS.WHITE,
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Form(
        child: SingleChildScrollView(
          child: Column(
            children: [
              _buildTextFieldUsername(),
              AppValue.vSpaceTiny,
              _buildTextFieldPassword(),
              AppValue.vSpaceSmall,
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _buildRememberPassword(),
                    _buildForgotPassword(),
                  ],
                ),
              ),
              AppValue.vSpaceSmall,
              _buildButtonLogin(),
              AppValue.vSpaceTiny,
              Text(
                MESSAGES.OR,
                style: AppStyle.DEFAULT_SMALL,
              ),
              AppValue.vSpaceTiny,
              _buildButtonLoginGoogle(),
              AppValue.vSpaceTiny,
              _buildButtonLoginFacebook()
            ],
          ),
        ),
      ),
    );
  }

  _buildForgotPassword() {
    return InkWell(
      onTap: () => null,
      child: Text(
        MESSAGES.FORGOT_PASSWORD,
        style: AppStyle.DEFAULT_SMALL_BOLD.copyWith(color: COLORS.PRIMARY_COLOR),
      ),
    );
  }

  bool _isSelected = false;

  _buildRememberPassword() {
    return InkWell(
      onTap: (){
        setState(() {
          _isSelected = !_isSelected;
        });
      },
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _isSelected ?
              WidgetContainerImage(
                height: 15,
                width: 15,
                image: ICONS.ICON_NOT_CHECK,
              ) :
              WidgetContainerImage(
                height: 15,
                width: 15,
                image: ICONS.ICON_CHECKED,
              ),
            AppValue.hSpaceTiny,
            Text(
              MESSAGES.REMEMBER_PASSWORD,
              style: AppStyle.DEFAULT_SMALL,
            ),
          ],
        ),
      ),
    );
  }

  _buildButtonLogin() {
    return WidgetButton(
      onTap: () => null,
      enable: true,
      backgroundColor: COLORS.PRIMARY_COLOR,
      text: MESSAGES.LOGIN,
    );
  }

  _buildButtonLoginGoogle() {
    return WidgetButton(
      onTap: () => null,
      enable: true,
      backgroundColor: COLORS.LOGIN_GG,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          WidgetContainerImage(
            image: ICONS.ICON_GG,
          ),
          Text(MESSAGES.LOGIN_WITH_GG, style: AppStyle.DEFAULT_MEDIUM.copyWith(color: COLORS.WHITE))
        ],
      ),
    );
  }

  _buildButtonLoginFacebook() {
    return WidgetButton(
      onTap: () => null,
      enable: true,
      backgroundColor: COLORS.LOGIN_FB,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          WidgetContainerImage(
            image: ICONS.ICON_FB,
          ),
          Text(MESSAGES.LOGIN_WITH_FB, style: AppStyle.DEFAULT_MEDIUM.copyWith(color: COLORS.WHITE))
        ],
      ),
    );
  }

  _buildTextFieldPassword() {
    return WidgetInput(
      onChanged: (value) => null,
      hint: MESSAGES.PASSWORD,
      obscureText: obscurePassword,
      focusNode: _passwordFocusNode,
      endIcon: InkWell(
        onTap: () => setState(() => obscurePassword = !obscurePassword),
        child: Icon(
          obscurePassword
              ? CommunityMaterialIcons.eye_outline
              : CommunityMaterialIcons.eye_off_outline,
          color: COLORS.GREY,
          size: 20,
        ),
      ),
    );
  }

  _buildTextFieldUsername() {
    return WidgetInput(
      onChanged: (value) => null,
      inputType: TextInputType.emailAddress,
      focusNode: _emailFocusNode,
      hint: MESSAGES.EMAIL_OR_PHONE,
    );
  }
}
