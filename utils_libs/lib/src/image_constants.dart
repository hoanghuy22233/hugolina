class IMAGES {
  static const String LOGO_APP = 'assets/images/logo_512.png';
  static const String LOGO_APP_FULL = 'assets/images/logo_full.png';
  static const String POST_IMAGE = 'assets/images/post_image.png';
}