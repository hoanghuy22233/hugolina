// ignore: import_of_legacy_library_into_null_safe
import 'package:hugolina/screens/screens.dart';
import 'package:hugolina/widgets/widget_button.dart';
import 'package:utils_libs/utils_libs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class UserScreen extends StatefulWidget {

  UserScreen({Key? key}) : super(key: key);

  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      body: SafeArea(
        child: WidgetContainerCenter(
          child: WidgetButton(
            onTap: (){
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return LoginAndRegisterScreen();
                },
              );
            },
            text: MESSAGES.LOGIN,
          ),
        ),
      ),
    );
  }
}
