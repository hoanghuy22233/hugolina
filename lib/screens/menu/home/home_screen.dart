// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter/cupertino.dart';
import 'package:hugolina/widgets/widgets.dart';
import 'package:utils_libs/utils_libs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'index.dart';

class HomeScreen extends StatefulWidget {

  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  List<String> level = [MESSAGES.DROPDOWN_SELECT_LEVEL, 'Dễ', 'Trung bình', 'Khó'];

  List<DropdownMenuItem<String>> buildDropdownMenuItems() {
    List<DropdownMenuItem<String>> items = [];
    for (String item in level) {
      items.add(DropdownMenuItem(value: item, child: Text(item, style: AppStyle.DEFAULT_SMALLs,)));
    }
    return items;
  }

  String _value = MESSAGES.DROPDOWN_SELECT_LEVEL;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: COLORS.WHITE,
          padding: EdgeInsets.all(10),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SearchWidget(),
                AppValue.vSpaceTiny,
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: WidgetImageCircle(
                              height: 35,
                              width: 35,
                            ),
                          ),
                          Expanded(
                            flex: 6,
                            child: WidgetButton(
                              height: 30,
                              onTap: (){},
                              text: MESSAGES.TITLE,
                            ),
                          ),
                          AppValue.hSpaceTiny,
                          Expanded(
                            flex: 4,
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              height: 30,
                              decoration: BoxDecoration(
                                border: Border.all(width: 0.5, color: COLORS.BLACK),
                                borderRadius: BorderRadius.circular(3),
                              ),
                              width: AppValue.widths,
                              child: DropdownButton(
                                items: buildDropdownMenuItems(),
                                icon: const Icon(Icons.keyboard_arrow_down),
                                onChanged: (String? value) {
                                  setState(() {
                                    _value = value!;
                                  });
                                },
                                value: _value,
                                isExpanded: true,
                                underline: Container(),
                                elevation: 2,
                                style: AppStyle.DEFAULT_SMALLs,
                                isDense: false,
                              ),
                            ),
                          )
                        ],
                      ),
                      AppValue.vSpaceSmall,
                      Text(MESSAGES.POST_CONTENT),
                      Divider(
                        thickness: 1,
                      ),
                      Container(
                        height: 40,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: [
                            WidgetRowTwoItem(
                              widgetLeft: WidgetContainerImage(height: 25, width: 25, image: ICONS.ICON_VIDEO,),
                              textRight: MESSAGES.VIDEO,
                            ),
                            AppValue.hSpaceMedium,
                            WidgetRowTwoItem(
                              widgetLeft: WidgetContainerImage(height: 25, width: 25, image: ICONS.ICON_IMAGE,),
                              textRight: MESSAGES.IMAGE,
                            ),
                            AppValue.hSpaceMedium,
                            WidgetRowTwoItem(
                              widgetLeft: WidgetContainerImage(height: 25, width: 25, image: ICONS.ICON_AUDIO,),
                              textRight: MESSAGES.AUDIO,
                            ),
                            AppValue.hSpaceMedium,
                            WidgetRowTwoItem(
                              widgetLeft: WidgetContainerImage(height: 25, width: 25, image: ICONS.ICON_VOCABULARY,),
                              textRight: MESSAGES.VOCABULARY,
                            ),
                            AppValue.hSpaceMedium,
                            WidgetRowTwoItem(
                              widgetLeft: WidgetContainerImage(height: 25, width: 25, image: ICONS.ICON_QUESTION,),
                              textRight: MESSAGES.QUESTION,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Divider(
                  thickness: 5 ,
                ),
                ListView(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    WidgetPostItem(),
                    WidgetPostItem(),
                    WidgetPostItem(),
                    WidgetPostItem(),
                  ],
                )
              ],
            ),
          ),
        )
      ),
    );
  }
}
