class ICONS {
  static const String HOME = 'assets/icons/home.png';
  static const String DISCOVER = 'assets/icons/discover.png';
  static const String COMMUNITY = 'assets/icons/community.png';
  static const String NOTIFICATION = 'assets/icons/notification.png';
  static const String USER = 'assets/icons/user.png';
  static const String ICON_BACK = 'assets/icons/icon_back.png';
  static const String ICON_GG = 'assets/icons/google_plus.png';
  static const String ICON_FB = 'assets/icons/facebook.png';
  static const String ICON_CHECKED = 'assets/icons/check.png';
  static const String ICON_NOT_CHECK = 'assets/icons/not_check.png';
  static const String ICON_VIDEO = 'assets/icons/video.png';
  static const String ICON_IMAGE = 'assets/icons/image.png';
  static const String ICON_AUDIO = 'assets/icons/audio.png';
  static const String ICON_VOCABULARY = 'assets/icons/vocabulary.png';
  static const String ICON_QUESTION = 'assets/icons/question.png';
  static const String PLAY_AUDIO = 'assets/icons/play_audio.png';
  static const String ICON_LIKE = 'assets/icons/like.png';
  static const String ICON_UNLIKE = 'assets/icons/un_like.png';
  static const String ICON_COMMENT = 'assets/icons/comment.png';
  static const String ICON_USEFUL = 'assets/icons/useful.png';
  static const String ICON_UN_USEFUL = 'assets/icons/un_useful.png';
  static const String ICON_SHARE = 'assets/icons/share.png';
  static const String ICON_LIKE_GIF = 'assets/icons/like.gif';
  static const String ICON_LOVE_GIF = 'assets/icons/love.gif';
  static const String ICON_WOW_GIF = 'assets/icons/wow.gif';
  static const String ICON_HAHA_GIF = 'assets/icons/haha.gif';
  static const String ICON_SAD_GIF = 'assets/icons/sad.gif';
  static const String ICON_ANGRY_GIF = 'assets/icons/angry.gif';
  static const String ICON_LOVE = 'assets/icons/love2.png';
  static const String ICON_WOW = 'assets/icons/wow2.png';
  static const String ICON_HAHA = 'assets/icons/haha2.png';
  static const String ICON_SAD = 'assets/icons/sad2.png';
  static const String ICON_ANGRY = 'assets/icons/angry2.png';
}